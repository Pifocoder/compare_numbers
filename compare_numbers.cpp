#include <iostream>
#include <string>

struct Number{
  std::string number_int = "";
  std::string number_double = "";
  bool sign;
};

bool CheckSign(Number& first, Number& second) {
  if ((first.sign == true) && (second.sign == false)) {
    return false;
  }

  if ((first.sign == true) && (second.sign == true)) {
    std::swap(first, second);
    first.sign = false;
    second.sign = false;
  }

  return true;
}
bool CheckInt(Number& first, Number& second) {
  if (first.number_int < second.number_int) {
    return false;
  }

  return true;
}
bool CheckDouble(Number& first, Number& second) {
  while (first.number_double.size() < second.number_double.size()) {
    first.number_double += "0";
  }

  while (first.number_double.size() > second.number_double.size()) {
    second.number_double += "0";
  }

  if (first.number_double < second.number_double) {
    return false;
  }

  return true;
}

void Input(Number& number) {
  std::string data;
  std::cin >> data;

  number.sign = (data[0] == '-');
  number.number_int = "";
  number.number_double = "";

  int number_digit = 0;
  if (number.sign) {
    ++number_digit;
  }

  for (number_digit = 0; number_digit < data.size(); ++number_digit) {
    if (data[number_digit] == ',') {
      ++number_digit;
      break;
    }
    number.number_int += data[number_digit];
  }

  for (; number_digit < data.size(); ++number_digit) {
    number.number_double += data[number_digit];
  }
}
std::string Solve() {
  Number first;
  Number second;
  Input(first);
  Input(second);

  bool first_not_lower = CheckSign(first, second);
  bool second_not_lower = CheckSign(second, first);
  if (first_not_lower != second_not_lower) {
    return first_not_lower ? ">" : "<";
  }
  
  first_not_lower = CheckInt(first, second);
  second_not_lower = CheckInt(second, first);
  if (first_not_lower != second_not_lower) {
    return first_not_lower ? ">" : "<";
  }

  first_not_lower = CheckDouble(first, second);
  second_not_lower = CheckDouble(second, first);
  if (first_not_lower != second_not_lower) {
    return first_not_lower ? ">" : "<";
  }

  return "=";
}
int main() {
  std::cout << Solve();
}